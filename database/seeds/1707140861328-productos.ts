import { Producto } from 'src/application/productos/entity'
import { USUARIO_SISTEMA } from 'src/common/constants'
import { MigrationInterface, QueryRunner } from 'typeorm'

export class Productos1707140861328 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    // TIPO DOCUMENTO
    const items = [
      {
        codigo: 'P0002',
        nombre: 'Refrigerador',
        descripcion: 'Refrigerador 20 pies cúbicos',
        precio: 1800,
        cantidad: 30,
      },
      {
        codigo: 'P0003',
        nombre: 'Laptop',
        descripcion: 'Laptop HP 15 pulgadas',
        precio: 1200,
        cantidad: 40,
      },
      {
        codigo: 'P0004',
        nombre: 'Cámara',
        descripcion: 'Cámara digital 24MP',
        precio: 800,
        cantidad: 25,
      },
      {
        codigo: 'P0005',
        nombre: 'Smartphone',
        descripcion: 'Smartphone Android 6.5 pulgadas',
        precio: 600,
        cantidad: 60,
      },
      {
        codigo: 'P0006',
        nombre: 'Tablet',
        descripcion: 'Tablet Samsung 10 pulgadas',
        precio: 400,
        cantidad: 35,
      },
      {
        codigo: 'P0007',
        nombre: 'Impresora',
        descripcion: 'Impresora láser monocromática',
        precio: 300,
        cantidad: 20,
      },
      {
        codigo: 'P0008',
        nombre: 'Aspiradora',
        descripcion: 'Aspiradora sin bolsa',
        precio: 150,
        cantidad: 50,
      },
      {
        codigo: 'P0009',
        nombre: 'Altavoces',
        descripcion: 'Altavoces Bluetooth',
        precio: 100,
        cantidad: 45,
      },
      {
        codigo: 'P0010',
        nombre: 'Teclado y ratón',
        descripcion: 'Combo teclado y ratón inalámbricos',
        precio: 50,
        cantidad: 55,
      },
    ]
    const productos = items.map((item) => {
      return new Producto({
        codigo: item.codigo,
        nombre: item.nombre,
        descripcion: item.descripcion,
        precio: item.precio,
        cantidad: item.cantidad,
        estado: 'ACTIVO',
        transaccion: 'SEEDS',
        usuarioCreacion: USUARIO_SISTEMA,
      })
    })
    await queryRunner.manager.save(productos)
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
