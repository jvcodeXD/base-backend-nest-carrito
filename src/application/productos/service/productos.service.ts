import { BaseService } from '../../../common/base/base-service'
import { Inject, Injectable, NotFoundException } from '@nestjs/common'
import { ProductoRepository } from '../repository'
import { CrearProductoDto } from '../dto'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { ActualizarProductoDto } from '../dto'
import { Status } from '../../../common/constants'
import { Messages } from '../../../common/constants/response-messages'

@Injectable()
export class ProductosService extends BaseService {
  constructor(
    @Inject(ProductoRepository)
    private productoRepositorio: ProductoRepository
  ) {
    super()
  }

  async crear(productoDto: CrearProductoDto, usuarioAuditoria: string) {
    return await this.productoRepositorio.crear(productoDto, usuarioAuditoria)
  }

  async listar(paginacionQueryDto: PaginacionQueryDto) {
    return await this.productoRepositorio.listar(paginacionQueryDto)
  }

  async actualizar(
    id: string,
    productoDto: ActualizarProductoDto,
    usuarioAuditoria: string
  ) {
    const producto = await this.productoRepositorio.buscarPorId(id)
    if (!producto) {
      throw new NotFoundException('No existe el producto')
    }
    await this.productoRepositorio.actualizar(id, productoDto, usuarioAuditoria)
    return { id }
  }

  async buscarPorId(id: string) {
    return await this.productoRepositorio.buscarPorId(id)
  }

  async activar(idProducto: string, usuarioAuditoria: string) {
    const producto = await this.productoRepositorio.buscarPorId(idProducto)
    if (!producto) {
      throw new NotFoundException(Messages.NO_PERMISSION_FOUND)
    }

    const productoDto = new CrearProductoDto()
    productoDto.estado = Status.ACTIVE
    await this.productoRepositorio.actualizar(
      idProducto,
      productoDto,
      usuarioAuditoria
    )
    return { id: idProducto, estado: productoDto.estado }
  }

  async inactivar(idProducto: string, usuarioAuditoria: string) {
    const producto = await this.productoRepositorio.buscarPorId(idProducto)
    if (!producto) {
      throw new NotFoundException(Messages.NO_PERMISSION_FOUND)
    }

    const productoDto = new CrearProductoDto()
    productoDto.estado = Status.INACTIVE
    await this.productoRepositorio.actualizar(
      idProducto,
      productoDto,
      usuarioAuditoria
    )
    return { id: idProducto, estado: productoDto.estado }
  }
}
