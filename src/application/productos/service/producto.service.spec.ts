import { Test, TestingModule } from '@nestjs/testing'
import { ProductosService } from './productos.service'
import { ProductoRepository } from '../repository'
import { TextService } from '../../../common/lib/text.service'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { ActualizarProductoDto, CrearProductoDto } from '../dto'
import { plainToClass } from 'class-transformer'
import { NotFoundException } from '@nestjs/common'

const resProducto = {
  id: TextService.generateUuid(),
  codigo: '123jkl',
  nombre: 'Galletas',
  descripcion: 'Galletas de agua',
  precio: 5,
  estado: 'ACTIVO',
}
const resProductoArray = [
  {
    id: TextService.generateUuid(),
    codigo: '123dfg',
    nombre: 'Galletas',
    descripcion: 'Galletas de agua',
    precio: 5,
    estado: 'ACTIVO',
  },
  {
    id: TextService.generateUuid(),
    codigo: '123abcd',
    nombre: 'Refresco',
    descripcion: 'Refresco sabor pomelo',
    precio: 9,
    estado: 'ACTIVO',
  },
]

const mockRequest: any = {
  user: {
    id: TextService.generateUuid(),
  },
}

describe('ProductoService', () => {
  let service: ProductosService
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProductosService,
        {
          provide: ProductoRepository,
          useValue: {
            crear: jest.fn(() => resProducto),
            listar: jest.fn(() => resProductoArray),
            actualizar: jest.fn().mockResolvedValueOnce(resProducto),
            buscarPorId: jest.fn(() => resProducto),
            buscarCodigo: jest.fn(() => resProducto),
          },
        },
      ],
    }).compile()

    service = module.get<ProductosService>(ProductosService)
  })

  it('[listar] lista de productos', async () => {
    const paginacion = new PaginacionQueryDto()
    const productos = await service.listar(paginacion)
    //console.log(`🌲 lista de productos`, productos)

    expect(productos).toBeInstanceOf(Array)
    expect(productos.length).toEqual(2)
  })

  it('[crear] crear pedido', async () => {
    const producto = new CrearProductoDto()
    producto.codigo = resProducto.codigo
    producto.nombre = resProducto.nombre
    producto.precio = resProducto.precio
    producto.descripcion = resProducto.descripcion

    const usuarioAuditoria = TextService.generateUuid()
    const result = await service.crear(producto, usuarioAuditoria)
    //console.log(`🔥 test unitario service - producto creado`, result)

    expect(result).toBeDefined()
    expect(result.codigo).toEqual(producto.codigo)
  })

  it('[actualizar] actualizar producto', async () => {
    const idProducto = '1'
    const body = plainToClass(ActualizarProductoDto, resProducto)
    const result = await service.actualizar(idProducto, body, mockRequest)
    //console.log(`🌲 actualizar producto`, result)

    expect(result).toBeDefined()
    expect(result).toHaveProperty('id')
  })

  it('[inactivar] inactivar producto', async () => {
    try {
      const idProducto = '1'
      const usuarioAuditoria = '2'
      const producto = await service.inactivar(idProducto, usuarioAuditoria)
      //console.log(`🌲 inactivar producto`, producto)
      expect(producto).toBeDefined()
      expect(producto).toHaveProperty('id')
      expect(producto).toHaveProperty('estado')
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException)
    }
  })

  it('[activar] activar producto', async () => {
    try {
      const idProducto = '1'
      const usuarioAuditoria = '2'
      const producto = await service.activar(idProducto, usuarioAuditoria)
      //console.log(`🌲 activar producto`, producto)
      expect(producto).toBeDefined()
      expect(producto).toHaveProperty('id')
      expect(producto).toHaveProperty('estado')
    } catch (error) {
      expect(error).toBeInstanceOf(NotFoundException)
    }
  })
})
