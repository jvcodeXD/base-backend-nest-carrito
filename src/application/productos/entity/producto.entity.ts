import { UtilService } from '../../../common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '../../../common/entity/auditoria.entity'
import { ProductoEstado } from '../constant'

dotenv.config()

@Check(UtilService.buildStatusCheck(ProductoEstado))
@Entity({ name: 'productos', schema: process.env.DB_SCHEMA_PRODUCTOS })
export class Producto extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Producto',
  })
  id: string

  @Column({
    length: 15,
    type: 'varchar',
    comment: 'Código de producto',
  })
  codigo: string

  @Column({ length: 50, type: 'varchar', comment: 'Nombre de producto' })
  nombre: string

  @Column({ length: 255, type: 'varchar', comment: 'Descripción de producto' })
  descripcion: string

  @Column({ type: 'float', comment: 'Precio unitario' })
  precio: number

  @Column({ type: 'int', comment: 'Cantidad disponible' })
  cantidad: number

  constructor(data?: Partial<Producto>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || ProductoEstado.ACTIVO
  }
}
