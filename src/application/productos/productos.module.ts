import { Module } from '@nestjs/common'
import { ProductosController } from './controller'
import { ProductosService } from './service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ProductoRepository } from './repository'
import { Producto } from './entity'

@Module({
  controllers: [ProductosController],
  providers: [ProductosService, ProductoRepository],
  imports: [TypeOrmModule.forFeature([Producto])],
  exports: [ProductosService],
})
export class ProductosModule {}
