import { Brackets, DataSource } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { ActualizarProductoDto, CrearProductoDto } from '../dto'
import { Producto } from '../entity'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'

@Injectable()
export class ProductoRepository {
  constructor(private dataSource: DataSource) {}

  async crear(productoDto: CrearProductoDto, usuarioAuditoria: string) {
    const { codigo, nombre, precio, cantidad, descripcion } = productoDto

    const producto = new Producto()
    producto.codigo = codigo
    producto.nombre = nombre
    producto.descripcion = descripcion
    producto.precio = precio
    producto.cantidad = cantidad
    producto.usuarioCreacion = usuarioAuditoria

    return await this.dataSource.getRepository(Producto).save(producto)
  }

  async listar(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar, filtro } = paginacionQueryDto
    const query = this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .select([
        'producto.id',
        'producto.codigo',
        'producto.nombre',
        'producto.precio',
        'producto.descripcion',
        'producto.cantidad',
        'producto.estado',
      ])
      .take(limite)
      .skip(saltar)

    query.addOrderBy('producto.id', 'ASC')

    if (filtro) {
      query.andWhere(
        new Brackets((qb) => {
          qb.orWhere('producto.codigo like :filtro', { filtro: `%${filtro}%` })
          qb.orWhere('producto.nombre ilike :filtro', {
            filtro: `%${filtro}%`,
          })
        })
      )
    }

    return await query.getManyAndCount()
  }

  async buscarCodigo(codigo: string) {
    return this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .where({ where: { codigo: codigo } })
      .getOne()
  }

  async buscarPorId(id: string) {
    return await this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .where({ id: id })
      .getOne()
  }

  async actualizar(
    id: string,
    productoDto: ActualizarProductoDto,
    usuarioAuditoria: string
  ) {
    return await this.dataSource
      .getRepository(Producto)
      .update(
        id,
        new Producto({ ...productoDto, usuarioModificacion: usuarioAuditoria })
      )
  }
}
