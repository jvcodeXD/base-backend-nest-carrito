import { Test, TestingModule } from '@nestjs/testing'
import { TextService } from '../../../common/lib/text.service'
import { ProductosController } from './productos.controller'
import { ProductosService } from '../service'
import { CanActivate } from '@nestjs/common'
import { CasbinGuard } from '../../../core/authorization/guards/casbin.guard'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { plainToClass } from 'class-transformer'
import { ActualizarProductoDto, CrearProductoDto } from '../dto'

const resProducto = {
  id: TextService.generateUuid(),
  codigo: 'asd123',
  nombre: 'Chocolate',
  descripcion: 'chocolate de 500 gr.',
  precio: 10.5,
}
const resListar = [1, resProducto]

const mockRequest: any = {
  user: {
    id: TextService.generateUuid(),
  },
}

describe('ProductoController', () => {
  let controller: ProductosController
  beforeAll(async () => {
    const mock_ForceFailGuard: CanActivate = {
      canActivate: jest.fn(() => true),
    }
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductosController],
      providers: [
        {
          provide: ProductosService,
          useValue: {
            crear: jest.fn(() => resProducto),
            listar: jest.fn(() => resListar),
            actualizar: jest.fn(() => resProducto),
            activar: jest.fn(() => resProducto),
            inactivar: jest.fn(() => resProducto),
          },
        },
      ],
    })
      .overrideGuard(CasbinGuard)
      .useValue(mock_ForceFailGuard)
      .compile()

    controller = module.get<ProductosController>(ProductosController)
  })

  it('[listar] lista de productos por id', async () => {
    const pagination = new PaginacionQueryDto()
    const result = await controller.listar(pagination)
    //console.log(`🔥 lista de productos`, result)

    expect(result).toBeDefined()
    expect(result).toHaveProperty('finalizado')
    expect(result).toHaveProperty('mensaje')
    expect(result).toHaveProperty('datos')
    expect(result.datos).toHaveProperty('total')
    expect(result.datos).toHaveProperty('filas')
  })

  it('[crear] crear producto', async () => {
    const productoDto = plainToClass(CrearProductoDto, resProducto)
    const result = await controller.crear(mockRequest, productoDto)
    //console.log(`🔥 Producto registrado`, result)

    expect(result).toBeDefined()
    expect(result).toHaveProperty('finalizado')
    expect(result).toHaveProperty('mensaje')
    expect(result).toHaveProperty('datos')
    expect(result.datos).toHaveProperty('id')
  })

  it('[actualizar] actualizar producto', async () => {
    const idProducto = { id: '1' }
    const body = plainToClass(ActualizarProductoDto, resProducto)
    const result = await controller.actualizar(idProducto, mockRequest, body)
    //console.log(`🔥 producto controller actualizar -> `, result)

    expect(result).toBeDefined()
    expect(result).toHaveProperty('finalizado')
    expect(result.finalizado).toEqual(true)
  })
  it('[activar] Debería activar un producto', async () => {
    const producto = await controller.activar(mockRequest, resProducto)
    //console.log(`🚨 Activar producto`, producto)
    expect(producto).toBeDefined()
    expect(producto).toHaveProperty('finalizado')
    expect(producto.finalizado).toEqual(true)
  })

  it('[inactivar] Debería inactivar un usuario', async () => {
    const param = {
      id: TextService.generateUuid(),
    }
    const producto = await controller.inactivar(mockRequest, param)
    //console.log(`🚨 Inactivar producto`, producto)
    expect(producto).toBeDefined()
    expect(producto).toHaveProperty('finalizado')
    expect(producto.finalizado).toEqual(true)
  })
})
