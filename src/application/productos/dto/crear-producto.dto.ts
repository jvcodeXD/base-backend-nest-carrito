import { IsNotEmpty } from '../../../common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class CrearProductoDto {
  @ApiProperty({ example: 'P0001' })
  @IsNotEmpty()
  codigo: string

  @ApiProperty({ example: 'Nombre del producto' })
  @IsNotEmpty()
  nombre: string

  @ApiProperty({ example: '1000' })
  @IsNotEmpty()
  precio: number

  @ApiProperty({ example: 'Descripción del producto' })
  descripcion: string

  @ApiProperty({ example: '50' })
  cantidad: number

  estado?: string
}

export class RespuestaCrearProductoDto {
  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  id: string
}
