import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty } from '../../../common/validation'

export class ActualizarProductoDto {
  @ApiProperty({ example: 'P0001' })
  @IsNotEmpty()
  codigo: string

  @ApiProperty({ example: 'Nombre del producto' })
  @IsNotEmpty()
  nombre: string

  @ApiProperty({ example: '1000' })
  @IsNotEmpty()
  precio: number

  @ApiProperty({ example: '50' })
  cantidad: number

  @ApiProperty({ example: 'Descripción del producto' })
  descripcion: string
}
