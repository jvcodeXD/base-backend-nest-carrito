import { BaseService } from '../../../common/base/base-service'
import { Inject, Injectable } from '@nestjs/common'
import { PedidoRepository } from '../repository/pedido.repository'
import { CrearPedidoDto, PedidoFiltro } from '../dto'

@Injectable()
export class PedidoService extends BaseService {
  constructor(
    @Inject(PedidoRepository)
    private pedidoRepositorio: PedidoRepository
  ) {
    super()
  }

  async crear(datosPedido: CrearPedidoDto, usuarioAuditoria: string) {
    return await this.pedidoRepositorio.crear(datosPedido, usuarioAuditoria)
  }

  async listar(filtro: PedidoFiltro) {
    const result = await this.pedidoRepositorio.listar(filtro)
    return result
  }

  async listarByUsuario(idUsuario: string) {
    const result = await this.pedidoRepositorio.listarByUsuario(idUsuario)
    return result
  }
}
