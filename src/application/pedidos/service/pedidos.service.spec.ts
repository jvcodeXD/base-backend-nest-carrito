import { Test, TestingModule } from '@nestjs/testing'
import { PedidoService } from './pedidos.service'
import { PedidoRepository } from '../repository'
import { TextService } from '../../../common/lib/text.service'
import { CrearPedidoDto, PedidoFiltro } from '../dto'
import dayjs from 'dayjs'
import { DataSource } from 'typeorm'

const resPedido = {
  id: TextService.generateUuid(),
  nroPedido: 1,
  fechaPedido: dayjs(new Date(), 'YYYY-MM-DD').toDate().toDateString(),
}

const resPedidoArray = [
  {
    id: TextService.generateUuid(),
    nroPedido: 1,
    fechaPedido: dayjs(new Date(), 'YYYY-MM-DD').toDate().toDateString(),
  },
  {
    id: TextService.generateUuid(),
    nroPedido: 2,
    fechaPedido: dayjs(new Date(), 'YYYY-MM-DD').toDate().toDateString(),
  },
]

describe('PedidoService', () => {
  let service: PedidoService
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PedidoService,
        {
          provide: PedidoRepository,
          useValue: {
            crear: jest.fn(() => resPedido),
            listar: jest.fn(() => [resPedidoArray, 1]),
          },
        },
        {
          provide: DataSource,
          useValue: {},
        },
      ],
    }).compile()

    service = module.get<PedidoService>(PedidoService)
  })

  it('[listar] Lista de pedidos', async () => {
    const paginacion = new PedidoFiltro()
    const pedidos = await service.listar(paginacion)
    //console.log(`🌲 test unitario service - lista de pedidos -> `, pedidos)

    expect(pedidos).toBeInstanceOf(Array)
    expect(pedidos.length).toEqual(2)
  })

  it('[crear] crear pedido', async () => {
    const pedido = new CrearPedidoDto()
    pedido.nroPedido = resPedido.nroPedido
    pedido.fechaPedido = resPedido.fechaPedido

    const usuarioAuditoria = TextService.generateUuid()
    const result = await service.crear(pedido, usuarioAuditoria)
    //console.log(`🔥 test unitario service - pedido creado`, result)

    expect(result).toBeDefined()
    expect(result.nroPedido).toEqual(pedido.nroPedido)
  })
})
