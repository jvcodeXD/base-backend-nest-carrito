import { Test, TestingModule } from '@nestjs/testing'
import { PedidosController } from './pedidos.controller'
import { PedidoService } from '../service'
import { CasbinGuard } from '../../../core/authorization/guards/casbin.guard'
import { CanActivate } from '@nestjs/common'
import { TextService } from '../../../common/lib/text.service'
import { CrearPedidoDto, PedidoFiltro } from '../dto'
import { plainToClass } from 'class-transformer'

const resPedido = {
  id: TextService.generateUuid(),
  nroPedido: 1,
  fechaPedido: new Date(),
}

const mockRequest: any = {
  user: {
    id: TextService.generateUuid(),
  },
}

const resListar = [1, resPedido]

describe('PedidosController', () => {
  let controller: PedidosController

  beforeAll(async () => {
    const mock_ForceFailGuard: CanActivate = {
      canActivate: jest.fn(() => true),
    }
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PedidosController],
      providers: [
        {
          provide: PedidoService,
          useValue: {
            crear: jest.fn(() => resPedido),
            listar: jest.fn(() => resListar),
          },
        },
      ],
    })
      .overrideGuard(CasbinGuard)
      .useValue(mock_ForceFailGuard)
      .compile()

    controller = module.get<PedidosController>(PedidosController)
  })

  it('[listar] test-unitario lista de pedidos', async () => {
    const pagination = new PedidoFiltro()
    const result = await controller.listar(pagination)
    //console.log(`🌲 lista obtenida -> `, result)
    expect(result).toBeDefined()
    expect(result).toHaveProperty('finalizado')
    expect(result).toHaveProperty('mensaje')
    expect(result).toHaveProperty('datos')
    expect(result.datos).toHaveProperty('total')
    expect(result.datos).toHaveProperty('filas')
  })

  it('[crear] test-unitario crear un nuevo pedido', async () => {
    const pedidoDto = plainToClass(CrearPedidoDto, resPedido)
    const result = await controller.crear(mockRequest, pedidoDto)
    //console.log(`🌲 pedido creado -> `, result)
    expect(result).toBeDefined()
    expect(result).toHaveProperty('finalizado')
    expect(result).toHaveProperty('mensaje')
    expect(result).toHaveProperty('datos')
    expect(result.datos).toHaveProperty('id')
  })
})
