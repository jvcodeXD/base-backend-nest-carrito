import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'

export class PedidoFiltro extends PaginacionQueryDto {
  fecha: string
}
