import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty } from 'class-validator'

export class CrearPedidoDto {
  @ApiProperty({ example: '4' })
  @IsNotEmpty()
  idUsuario: string

  @ApiProperty({ example: '0' })
  @IsNotEmpty()
  costo: number
}
