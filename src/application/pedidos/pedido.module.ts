import { Module } from '@nestjs/common'
import { PedidosController } from './controller'
import { PedidoService } from './service'
import { PedidoRepository } from './repository'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Pedido } from './entity'

@Module({
  controllers: [PedidosController],
  providers: [PedidoService, PedidoRepository],
  imports: [TypeOrmModule.forFeature([Pedido])],
})
export class PedidosModule {}
