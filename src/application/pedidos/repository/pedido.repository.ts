import { DataSource } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { Pedido } from '../entity'
import { CrearPedidoDto, PedidoFiltro } from '../dto'
import dayjs from 'dayjs'

@Injectable()
export class PedidoRepository {
  constructor(private dataSource: DataSource) {}

  async listar(filtroPedidos: PedidoFiltro) {
    const { limite, saltar, fecha } = filtroPedidos
    const query = this.dataSource
      .getRepository(Pedido)
      .createQueryBuilder('pedido')
      .select([
        'pedido.id', //
        'pedido.nroPedido',
        'pedido.idUsuario',
        'pedido.costo',
        'pedido.fechaPedido',
        'pedido.estado',
      ])
      .take(limite)
      .skip(saltar)

    if (fecha) {
      query.where('pedido.fechaPedido = :fecha', { fecha })
    }

    return await query.getManyAndCount()
  }

  async listarByUsuario(idUsuario: string) {
    const last = await this.dataSource
      .getRepository(Pedido)
      .createQueryBuilder('pedido')
      .select([
        'pedido.id',
        'pedido.nroPedido',
        'pedido.costo',
        'pedido.idUsuario',
      ])
      .where(`pedido.idUsuario = :idUsuario`, {
        idUsuario,
      })
      .orderBy('pedido.nroPedido', 'DESC')
      .getMany()
    console.log(last)

    return last
  }

  async crear(datosPedido: CrearPedidoDto, usuarioAuditoria: string) {
    const last = await this.dataSource
      .getRepository(Pedido)
      .createQueryBuilder('pedido')
      .select(['pedido.nroPedido'])
      .orderBy('pedido.nroPedido', 'DESC')
      .limit(1)
      .getOne()

    const datosGuardar = new Pedido({
      nroPedido: last ? last.nroPedido + 1 : 0,
      idUsuario: datosPedido.idUsuario,
      costo: datosPedido.costo,
      fechaPedido: dayjs().toDate(),
      usuarioCreacion: usuarioAuditoria,
    })
    return await this.dataSource.getRepository(Pedido).save(datosGuardar)
  }
}
