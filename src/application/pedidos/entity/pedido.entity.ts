import { UtilService } from '../../../common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '../../../common/entity/auditoria.entity'
import { PedidoEstado } from '../constant'
import { DetallePedido } from './detalle-pedido.entity'

dotenv.config()

@Check(UtilService.buildStatusCheck(PedidoEstado))
@Entity({ name: 'pedidos', schema: process.env.DB_SCHEMA_PEDIDOS })
export class Pedido extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Pedido',
  })
  id: string

  @Column({
    name: 'nro_pedido',
    type: 'int',
    unique: true,
    comment: 'Nro. de pedido',
  })
  nroPedido: number

  @Column({
    name: 'fecha_pedido',
    type: 'timestamp without time zone',
    comment: 'Fecha de Pedido',
  })
  fechaPedido: Date

  @Column({
    name: 'costo_total',
    type: 'int',
    comment: 'Costo total del pedido',
  })
  costo: number

  @Column({
    name: 'id_usuario',
    type: 'varchar',
    comment: 'Id del usuario que realiza el pedido',
  })
  idUsuario: string

  @OneToMany(() => DetallePedido, (detallePedido) => detallePedido.pedido)
  detalles: DetallePedido[]

  constructor(data?: Partial<Pedido>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || PedidoEstado.ACTIVO
  }
}
