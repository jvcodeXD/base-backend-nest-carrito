import { Module } from '@nestjs/common'
import { ParametricasModule } from './parametricas/parametricas.module'
import { ProductosModule } from './productos/productos.module'
import { PedidosModule } from './pedidos/pedido.module'

@Module({
  imports: [ParametricasModule, ProductosModule, PedidosModule],
})
export class ApplicationModule {}
